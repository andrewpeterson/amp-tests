#!/usr/bin/env python

import os
import sys
import time
import getpass
import virtualenv


def maketestdir(count=0):
    """Makes a temporary directory in scratch in which the code will
    compile and the tests will run."""
    user = getpass.getuser()
    scratchbase = os.path.join('/users', user, 'scratch')
    parent = os.path.join(scratchbase, 'amp-tests')
    if not os.path.exists(parent):
        os.mkdir(parent)
    now = time.localtime()
    testdir = os.path.join(scratchbase, 'amp-tests',
                           '%04i%02i%02i' % (now.tm_year, now.tm_mon,
                                             now.tm_mday))
    if count:
        testdir += '-%i' % count
    try:
        os.mkdir(testdir)
    except OSError:
        count += 1
        testdir = maketestdir(count)
    return testdir


if __name__ == "__main__":
    testdir = maketestdir()
    envdir = os.path.join(testdir, 'env')
    virtualenv.create_environment(home_dir=envdir,
                                  site_packages=False,)

    sys.stdout.write(testdir)

