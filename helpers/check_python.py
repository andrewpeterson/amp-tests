#!/usr/bin/env python
"""Checks that the correct local installs of necessary python packages are
in use. When called as script relies on stdin to get basepath."""

def check_environment(basepath):
    """Checks that the key imports each are coming from basepath."""
    import os
    assert os.__file__.startswith(basepath)
    #import numpy  
    #assert numpy.__file__.startswith(basepath)
    #import scipy
    #assert scipy.__file__.startswith(basepath)
    import ase
    assert ase.__file__.startswith(basepath)
    import amp 
    assert amp.__file__.startswith(basepath)

if __name__ == '__main__':
    import sys
    basepath = sys.argv[-1]

    # Check python environment.
    print('Python os imports from: ')
    import os
    print(os.__file__)

    print('Current working directory:')
    print(os.getcwd())

    #import numpy
    #print(numpy.__file__)

    #import scipy
    #print(scipy.__file__)

    import ase
    print('ASE imports from: ')
    print(ase.__file__)

    import amp
    print('Amp imports from: ')
    print(amp.__file__)

    check_environment(basepath)
