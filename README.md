# README #

This repository has scripts to help us automate the running of the tests for the Amp package. The script downloads the newest version of both Amp and ASE, then executes the tests from Amp.

It does this by using a cron job to submit the below script as with "sbatch path/to/testamp.sh":

File testamp.sh:
```
#!/bin/bash
#SBATCH --time=10:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --partition=maint-batch
#SBATCH --mail-type=ALL
#SBATCH --mail-user=your_email@yourhost
#
# This script submits the autotests for the amp package.
#
# Amp autotest package:
#  https://bitbucket.org/andrewpeterson/amp-tests
# Amp package:
#  https://bitbucket.org/andrewpeterson/amp

cd amp-tests
./autotest.sh
```
