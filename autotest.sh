
originaldirectory=`pwd`

#################################################################
# Custom modules needed for Oscar system at Brown.
#################################################################
module load gcc

#################################################################
# Create virtual environment (from python script).
#################################################################
testdir="`python helpers/create_environment.py`"
echo $testdir

#################################################################
# Activate virtual environment, zeroing out the pythonpath first.
#################################################################
cd $testdir
export PYTHONPATH=""
source env/bin/activate

#################################################################
# Install standard packages.
#################################################################
pip install -U setuptools
pip install -U pip
#pip install numpy
#pip install scipy
pip install -U nose
#pip install ffnet
pip install zmq
pip install pexpect


#################################################################
# Load module for GPU and install tensorflow.
#################################################################
# This is crashing for the moment. Shut it off.
#module load cuda
#export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow_gpu-0.12.1-cp27-none-linux_x86_64.whl
#pip install --upgrade $TF_BINARY_URL	


#################################################################
# Install latest ASE.
#################################################################
#pip install svn+https://svn.fysik.dtu.dk/projects/ase/trunk
pip install git+https://gitlab.com/ase/ase.git


#################################################################
# Install, compile, and set environment for latest Amp.
#################################################################
mkdir $testdir/ampcode
cd $testdir/ampcode
git clone git@bitbucket.org:andrewpeterson/amp.git amp
cd $testdir/ampcode/amp/amp/model
gfortran -c neuralnetwork.f90
mv neuralnetwork.mod ../
cd ../descriptor
gfortran -c cutoffs.f90
mv cutoffs.mod ../
cd ../
f2py -c -m fmodules model.f90 descriptor/cutoffs.f90 descriptor/gaussian.f90 descriptor/zernike.f90 model/neuralnetwork.f90
export PYTHONPATH=$testdir/ampcode/amp:$PYTHONPATH



#################################################################
# Check amp installation.
#################################################################
echo 'Checking installation.'
cd $originaldirectory
python $originaldirectory/helpers/check_python.py $testdir

#################################################################
# Prepare tests folder.
#################################################################
echo 'Preparing tests folder.'
mkdir $testdir/runtests
cd $testdir/runtests
cp -r $testdir/ampcode/amp/tests $testdir/runtests


#################################################################
# Run tests.
#################################################################
echo 'Starting nose tests.'
nosetests -v 2>&1 | tee autotest.log

#################################################################
# Email test results.
#################################################################

ssh login001 mailx -s \"Amptest $testdir.\" $amptestmail \<$testdir/runtests/autotest.log
